# Copyright (c) 2019-2022 Univ. Bordeaux #



# Open-sources resources for full reproducibility

All details to fully reproduce the results concerning the profiling of upcoming application profiles on HPC facilities presented in the paper "Profiles of upcoming HPC Applications
and their Impact on Reservation Strategies" (DOI: [10.1109/TPDS.2020.3039728](10.1109/TPDS.2020.3039728))

This repository contains all data and procedures to generate the results presented in the paper associated to profiling of applications and simulation of reservation strategies.


Folder [application](application) contains the description of the application, as so as the considered inputs in this work.

Folder [data](data) contains the data for each run of the application (memory footprints, makespan, extraction of data for application model).

Folder [figures](figures) describe how to generate the figures of Section II,III and IV.a) of the paper.
We provide all data manipulation scripts, plotting scripts and simulation code to generate all of them.
