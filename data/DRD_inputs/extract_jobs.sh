#!/bin/bash


#################### a first pass on each files
#################### some runs needs some other bound values and will be reprocessed after

path="../memory_footprint"
# list of input files
list_files=$(ls $path)

mem_values="3200000;9000000;5600000;9000000;45000000;3500000;10000000" ## ou 3200000 10000000 etc


for input_file in $list_files
do
	job_number=$(echo $input_file | tr -dc '0-9')
	echo Job $job_number

	rm -rf  jobs/$job_number
	mkdir jobs/$job_number
	
	firstline=$(head -n 1 $path/$input_file)
	mem1=$(echo $firstline | cut -d',' -f2)
	var=2
	((stop=$mem1*$var))
	stop=2800000

	if ((job_number==77))
	then
		stop=3000000
	fi


	task_counter=1
	bound=$(echo $mem_values | cut -d';' -f $task_counter)
	reach_bound=0

	counter_task5=0


	while IFS= read -r line
	do
		mem=$(echo $line | cut -d',' -f 2)
		# which memory limit am I looking for
		
		if ((task_counter==5)) # treat 27 iteration of 5th job to consider it as only one
		then
			if ((mem < stop && counter_task5==27)) #end task 5
			then
				((task_counter=$task_counter+1))
				reach_bound=0
				bound=$(echo $mem_values | cut -d';' -f $task_counter)
			else
				if ((mem < stop && reach_bound==1))
				then
					reach_bound=0
					((counter_task5=$counter_task5+1))
					#echo $counter_task5
				else
					if ((mem>=bound))
					then
						reach_bound=1
					fi
				fi
				echo $line >> jobs/$job_number/task$task_counter.csv
			fi
		else
			if ((task_counter!=7)) # for the last task, we dont care about increasing counter, just append until the end
			then
				if ((reach_bound==0 && mem>=bound))
				then
					reach_bound=1
				fi
				if ((mem < stop && reach_bound==1))
				then
					((task_counter=$task_counter+1))
					reach_bound=0
					bound=$(echo $mem_values | cut -d';' -f $task_counter)
				fi
			fi	
			echo $line >> jobs/$job_number/task$task_counter.csv
		fi
	done < "$path/$input_file"

	e=$(ls jobs/$job_number | wc -l)
	if ((e!=7))
	then
		echo Wrong number of parsed jobs for Job number $job_number
	fi
done



#################### some specific jobs need a particular treatment
#################### slight variations in bounds of each task

list_files="monitoring112.csv monitoring121.csv monitoring27.csv monitoring46.csv monitoring68.csv monitoring74.csv monitoring87.csv"

mem_values="3200000;10000000;5600000;9000000;45000000;3300000;10000000"


for input_file in $list_files
do
	job_number=$(echo $input_file | tr -dc '0-9')
	echo Job $job_number

	rm -rf  jobs/$job_number
	mkdir jobs/$job_number
	
	firstline=$(head -n 1 $path/$input_file)
	mem1=$(echo $firstline | cut -d',' -f2)
	var=2
	((stop=$mem1*$var))
	stop=2900000


	task_counter=1
	bound=$(echo $mem_values | cut -d';' -f $task_counter)
	reach_bound=0

	counter_task5=0


	while IFS= read -r line
	do
		mem=$(echo $line | cut -d',' -f 2)
		# which memory limit am I looking for
		
		if ((task_counter==5)) # treat 27 iteration of 5th job to consider it as only one
		then
			if ((mem < stop && counter_task5==26)) #end task 5
			then
				((task_counter=$task_counter+1))
				reach_bound=0
				bound=$(echo $mem_values | cut -d';' -f $task_counter)
			else
				if ((mem < stop && reach_bound==1))
				then
					reach_bound=0
					((counter_task5=$counter_task5+1))
					#echo $counter_task5
				else
					if ((mem>=bound))
					then
						reach_bound=1
					fi
				fi
				echo $line >> jobs/$job_number/task$task_counter.csv
			fi
		else
			if ((task_counter!=7)) # for the last task, we dont care about increasing counter, just append until the end
			then
				if ((reach_bound==0 && mem>=bound))
				then
					reach_bound=1
				fi
				if ((mem < stop && reach_bound==1))
				then
					((task_counter=$task_counter+1))
					reach_bound=0
					bound=$(echo $mem_values | cut -d';' -f $task_counter)
				fi
			fi	
			echo $line >> jobs/$job_number/task$task_counter.csv
		fi
	done < "$path/$input_file"

	e=$(ls jobs/$job_number | wc -l)
	if ((e!=7))
	then
		echo Wrong number of parsed jobs for Job number $job_number
	fi
done




#################### some specific jobs need a particular treatment
#################### slight variations in bounds of each task

path="../data/7tasks/data"
# list of input files
list_files=$(ls $path)

list_files="monitoring30.csv monitoring40.csv monitoring50.csv monitoring91.csv"

mem_values="3200000;9000000;5600000;9000000;45000000;3500000;10000000"


for input_file in $list_files
do
	job_number=$(echo $input_file | tr -dc '0-9')
	echo Job $job_number

	rm -rf  jobs/$job_number
	mkdir jobs/$job_number
	
	firstline=$(head -n 1 $path/$input_file)
	mem1=$(echo $firstline | cut -d',' -f2)
	var=2
	((stop=$mem1*$var))
	stop=2660000


	task_counter=1
	bound=$(echo $mem_values | cut -d';' -f $task_counter)
	reach_bound=0

	counter_task5=0

	while IFS= read -r line
	do
		mem=$(echo $line | cut -d',' -f 2)
		# which memory limit am I looking for
		
		if ((task_counter==5)) # treat 27 iteration of 5th job to consider it as only one
		then
			if ((mem < stop && counter_task5==27)) #end task 5
			then
				((task_counter=$task_counter+1))
				reach_bound=0
				bound=$(echo $mem_values | cut -d';' -f $task_counter)
			else
				if ((mem < stop && reach_bound==1))
				then
					reach_bound=0
					((counter_task5=$counter_task5+1))
					#echo $counter_task5
				else
					if ((mem>=bound))
					then
						reach_bound=1
					fi
				fi
				echo $line >> jobs/$job_number/task$task_counter.csv
			fi
		else
			if ((task_counter!=7)) # for the last task, we dont care about increasing counter, just append until the end
			then
				if ((reach_bound==0 && mem>=bound))
				then
					reach_bound=1
				fi
				if ((mem < stop && reach_bound==1))
				then
					((task_counter=$task_counter+1))
					reach_bound=0
					bound=$(echo $mem_values | cut -d';' -f $task_counter)
				fi
			fi	
			echo $line >> jobs/$job_number/task$task_counter.csv
		fi
	done < "$path/$input_file"

	e=$(ls jobs/$job_number | wc -l)
	if ((e!=7))
	then
		echo Wrong number of parsed jobs for Job number $job_number
	fi
done
